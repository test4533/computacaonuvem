from sqlalchemy import create_engine, MetaData, Table, Column
from sqlalchemy import Integer, Float, Boolean
from db_connect import engine

import numpy as np
import scipy.stats as stats
import pickle


# create table
meta = MetaData()
table1 = Table(
   'table1', meta,
   Column('id', Integer, primary_key = True),
   Column('f1', Float),
   Column('f2', Float),
   Column('y', Integer),
   Column('train', Boolean),
)
meta.create_all(engine)


# generate some data, supose this data come from the real world
n_train = 1000
n_test = 100
d = 2
beta = stats.norm.rvs(size=d)
func = lambda x: (np.dot(beta, x) + stats.norm.rvs(scale=2)) > 0

x_train = stats.norm.rvs(scale=3, size=n_train*d).reshape((n_train, d))
y_train = np.apply_along_axis(func, 1, x_train).astype(int)

x_test = stats.norm.rvs(scale=3, size=n_test*d).reshape((n_test, d))
y_test = np.apply_along_axis(func, 1, x_test).astype(int)

conn = engine.connect()

for x, y in zip(x_train, y_train):
    ins = table1.insert().values(f1 = x[0].item(), f2 = x[1].item(),
        y = y.item(), train = True)
    result = conn.execute(ins)

for x, y in zip(x_test, y_test):
    ins = table1.insert().values(f1 = x[0].item(), f2 = x[1].item(),
        y = y.item(), train = False)
    result = conn.execute(ins) 

