from sqlalchemy import create_engine, MetaData, Table, Column
from sqlalchemy import Integer, String

user = 'postgres'
password = 'Marco'
hostname = '34.135.121.26'
database_name = 'testedb'

# note: run this sql if neeeded: CREATE DATABASE testedb;
engine = create_engine(
f'postgresql+psycopg2://{user}:{password}@{hostname}/{database_name}')
