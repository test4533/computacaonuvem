from sqlalchemy import MetaData, Table, Column
from storage_functions import upload_blob, download_blob
from db_connect import engine

import numpy as np
import scipy.stats as stats
import pickle

bucket_name = "teste_aula_dt_cloud_marco"

# autoload table structure
meta = MetaData()
table1 = Table('table1', meta, autoload=True, autoload_with=engine)
conn = engine.connect()


sel = table1.select().where(table1.c.train==True)

result = conn.execute(sel)
_, f1, f2, y_train, _ = zip(*result)
x_train = np.column_stack((f1,f2))

sel = table1.select().where(table1.c.train==False)
result = conn.execute(sel)
_, f1, f2, y_test, _ = zip(*result)
x_test = np.column_stack((f1,f2))

with open("data.pkl", 'wb') as f:
    pickle.dump((x_train, x_test, y_train, y_test), f)

upload_blob(bucket_name, 'data.pkl', 'stored_data.pkl')
